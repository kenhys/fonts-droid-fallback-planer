# frozen_string_literal: true

require_relative "planer/version"

module Fonts
  module Droid
    module Fallback
      module Planer
        class Error < StandardError; end
        # Your code goes here...
      end
    end
  end
end
