# frozen_string_literal: true

module Fonts
  module Droid
    module Fallback
      module Planer
        VERSION = "0.1.0"
      end
    end
  end
end
