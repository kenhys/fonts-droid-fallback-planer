# frozen_string_literal: true

require "test_helper"

class Fonts::Droid::Fallback::PlanerTest < Test::Unit::TestCase
  test "VERSION" do
    assert do
      ::Fonts::Droid::Fallback::Planer.const_defined?(:VERSION)
    end
  end

  test "something useful" do
    assert_equal("expected", "actual")
  end
end
