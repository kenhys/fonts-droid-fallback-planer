# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "fonts/droid/fallback/planer"

require "test-unit"
